from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date='October 20 2015')

@mod.route('/delete', methods=['POST'])
def delete_post():
    g.postsdb.deletePost(request.form['post_to_delete'])
    redirect(url_for('.post_list'))
    flash('Post Deleted!', 'remove_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    emotion = request.form['emotions']
    g.postsdb.createPost(new_post, session['username'], emotion)
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))