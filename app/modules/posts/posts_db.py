class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def deletePost(self, oid):
        from bson.objectid import ObjectId
        return self.conn.remove({'_id':ObjectId(oid)})

    def createPost(self, post, username, emotion):
        self.conn.insert({'post':post, 'username': username, 'emotion':emotion })